//Top 10 economical bowlers in the given year
function top10EconomicalBowlers(matches, deliveries, year) {
    const matchesOfSeason = matches.reduce((matchIdWithSeason, match) => {
        if (match.season == year)
            matchIdWithSeason[match.id] = match.season;
        return matchIdWithSeason;
    }, {});

    const runsConceded = deliveries.reduce((runsConcededByBowler, delivery) => {
        if (matchesOfSeason.hasOwnProperty(delivery.match_id) && delivery.wide_runs == 0 && delivery.noball_runs == 0) {
            if (runsConcededByBowler.hasOwnProperty(delivery.bowler)) {
                runsConcededByBowler[delivery.bowler].runs += parseInt(delivery.total_runs);
                runsConcededByBowler[delivery.bowler].balls += 1;
                runsConcededByBowler[delivery.bowler].economy = runsConcededByBowler[delivery.bowler].runs / (runsConcededByBowler[delivery.bowler].balls / 6);
            }
            else {
                runsConcededByBowler[delivery.bowler] = {};
                runsConcededByBowler[delivery.bowler].runs = parseInt(delivery.total_runs);
                runsConcededByBowler[delivery.bowler].balls = 1;
                runsConcededByBowler[delivery.bowler].economy = runsConcededByBowler[delivery.bowler].runsConceded / (runsConcededByBowler[delivery.bowler].balls / 6);
            }
        }
        return runsConcededByBowler;
    }, {});

    const top10EconomicBowlers = Object.fromEntries(
        Object.entries(runsConceded).sort((bowler1, bowler2) => {
            if (bowler1[1].economy > bowler2[1].economy)
                return 1;
            else if (bowler1[1].economy < bowler2[1].economy)
                return -1;
            else
                return 0;
        }).slice(0, 10)
    );

    return top10EconomicBowlers;
}

module.exports = top10EconomicalBowlers;