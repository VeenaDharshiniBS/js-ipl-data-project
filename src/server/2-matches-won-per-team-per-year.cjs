//Number of matches won per team per year in IPL.
function matchesWonPerTeamPerYear(matches) 
{
    const wonTeams = matches.reduce((team,match)=>{
    if(team.hasOwnProperty(match.winner))
    {
        if(team[match.winner].hasOwnProperty(match.season))
            team[match.winner][match.season] += 1;
        else
            team[match.winner][match.season] = 1;
    }
    else
    {
        team[match.winner] = {};
        team[match.winner][match.season] = 1;
    }
    return team;
  },{});

  return wonTeams;
}

module.exports = matchesWonPerTeamPerYear;
