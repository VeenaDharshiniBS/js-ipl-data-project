//Find a player who has won the highest number of Player of the Match awards for each season
function playerOfTheMatches(matches) {
    const wonPlayers = matches.reduce((seasonWithPlayer, match) => {
        if (seasonWithPlayer.hasOwnProperty(match.season)) {
            if (seasonWithPlayer[match.season].hasOwnProperty(match.player_of_match))
                seasonWithPlayer[match.season][match.player_of_match] += 1;
            else
                seasonWithPlayer[match.season][match.player_of_match] = 1;
        }
        else {
            seasonWithPlayer[match.season] = {};
            seasonWithPlayer[match.season][match.player_of_match] = 1;
        }
        return seasonWithPlayer;
    }, {});

    let maxPlayerOfMatchPerSeason = Object.fromEntries(Object.keys(wonPlayers).map((year) => {
        let arrayOfCountsEachPlayerOwn = Object.values(wonPlayers[year]);
        let maxCount = Math.max(...arrayOfCountsEachPlayerOwn);
        let maxPlayer = [];
        Object.keys(wonPlayers[year]).map((player) => {
            if (wonPlayers[year][player] == maxCount)
                maxPlayer.push(player);
        });
        return [ year, {playerOfTheMatch: maxPlayer, count: maxCount} ];
    }));

    return maxPlayerOfMatchPerSeason;
}

module.exports = playerOfTheMatches;