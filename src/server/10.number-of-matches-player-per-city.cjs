//Number of matches played per city
function numberOfMatchesPlayerPerCity(matches)
{
    let countPerCity = matches.reduce((matchPerCity,match)=>{
        if(matchPerCity.hasOwnProperty(match.city) && match.city!="")
        {
            matchPerCity[match.city] += 1;
        }
        else if(match.city!="")
        {
            matchPerCity[match.city] = 1
        }
        return matchPerCity;
    },{});

    return countPerCity;
}

module.exports = numberOfMatchesPlayerPerCity;