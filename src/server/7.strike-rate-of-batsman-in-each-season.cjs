//Find the strike rate of a batsman for each season
function strikeRatesOfPerYear(matches, deliveries) {
    const matchesOfSeason = matches.reduce((matchIdWithSeason, match) => {
        matchIdWithSeason[match.id] = match.season;
        return matchIdWithSeason;
    }, {});

    const allBatsmanDetails = deliveries.reduce((batsmanDetails, delivery) => {
        let season = matchesOfSeason[delivery.match_id];
        if (batsmanDetails.hasOwnProperty(delivery.batsman)) {
            if (batsmanDetails[delivery.batsman].hasOwnProperty(season)) {
                batsmanDetails[delivery.batsman][season].totalRuns += parseInt(delivery.total_runs);
                batsmanDetails[delivery.batsman][season].totalBallsFaced += 1;
            }
            else {
                batsmanDetails[delivery.batsman][season] = {};
                batsmanDetails[delivery.batsman][season].totalRuns = parseInt(delivery.total_runs);
                batsmanDetails[delivery.batsman][season].totalBallsFaced = 1;
            }
        }
        else {
            batsmanDetails[delivery.batsman] = {};
            batsmanDetails[delivery.batsman][season] = {};
            batsmanDetails[delivery.batsman][season].totalRuns = parseInt(delivery.total_runs);
            batsmanDetails[delivery.batsman][season].totalBallsFaced = 1;
        }
        return batsmanDetails;
    }, {});

    const batsmanStrikeRatePerYear = {};

    Object.keys(allBatsmanDetails).map(batsman => {
        let strike = {};
        Object.keys(allBatsmanDetails[batsman]).map(year => {
            let strikeRate = (allBatsmanDetails[batsman][year].totalRuns / allBatsmanDetails[batsman][year].totalBallsFaced) * 100;
            strike[year] = strikeRate.toFixed(2);
        })
        batsmanStrikeRatePerYear[batsman] = strike;
    });

    return batsmanStrikeRatePerYear;

}
module.exports = strikeRatesOfPerYear;