//Find the bowler with the best economy in super overs
function bestEconomyInSuperOver(deliveries) {
    const superOverBowlers = deliveries.reduce((superOverBowler, delivery) => {
        if (delivery.is_super_over != 0) {
            if (superOverBowler.hasOwnProperty(delivery.bowler)) {
                superOverBowler[delivery.bowler].runsConceded += parseInt(delivery.total_runs);
                superOverBowler[delivery.bowler].balls += 1;
                superOverBowler[delivery.bowler].economy = superOverBowler[delivery.bowler].runsConceded / (superOverBowler[delivery.bowler].balls / 6);
            }
            else {
                superOverBowler[delivery.bowler] = {};
                superOverBowler[delivery.bowler].runsConceded = parseInt(delivery.total_runs);
                superOverBowler[delivery.bowler].balls = 1;
                superOverBowler[delivery.bowler].economy = superOverBowler[delivery.bowler].runsConceded / (superOverBowler[delivery.bowler].balls / 6);
            }
        }
        return superOverBowler;
    }, {});


    const bestEconomicBowler = Object.fromEntries(
        Object.entries(superOverBowlers).sort(([, bowler1], [, bowler2]) => {
            if (bowler1.economy > bowler2.economy)
                return 1;
            else if (bowler1.economy < bowler2.economy)
                return -1;
            else
                return 0;
        }).slice(0, 1)
    );

    return bestEconomicBowler;
}

module.exports = bestEconomyInSuperOver;
