//Find the highest number of times one player has been dismissed by another player
function highestPlayerDismissed(deliveries) {
    const playersDismissedByBowlers = deliveries.reduce((batsmanDismissed, delivery) => {

        if (batsmanDismissed.hasOwnProperty(delivery.player_dismissed) && delivery.player_dismissed != '') {
            if (batsmanDismissed[delivery.player_dismissed].hasOwnProperty(delivery.bowler)) {
                batsmanDismissed[delivery.player_dismissed][delivery.bowler] += 1;
            }
            else {
                batsmanDismissed[delivery.player_dismissed][delivery.bowler] = 1;
            }
        }
        else if (delivery.player_dismissed != '') {
            batsmanDismissed[delivery.player_dismissed] = {};
            batsmanDismissed[delivery.player_dismissed][delivery.bowler] = 1;
        }
        return batsmanDismissed;
    }
        , {});

    const batsmanDismmisedCounts = Object.keys(playersDismissedByBowlers).map((batsman) => {
        return Object.keys(playersDismissedByBowlers[batsman]).map((bowler) => {
            return playersDismissedByBowlers[batsman][bowler];
        });
    });

    const maxDismissedCount = Math.max(...batsmanDismmisedCounts.flat(Infinity));

    const maxDismissedBatsman = Object.keys(playersDismissedByBowlers).reduce((playerDismissedByBowlers, batsman) => {
        const bowlers = Object.keys(playersDismissedByBowlers[batsman]).filter((bowler) => {
            if (playersDismissedByBowlers[batsman][bowler] == maxDismissedCount)
                return true;
            else
                return false;
        });

        if (bowlers.length > 0) {
            playerDismissedByBowlers.push({
                [batsman]: {
                    'dismissedBowlers': bowlers,
                    'dismissedCount': maxDismissedCount
                }
            });
        }
        return playerDismissedByBowlers;
    }, []);

    return maxDismissedBatsman;
}

module.exports = highestPlayerDismissed;