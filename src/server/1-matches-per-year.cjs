//Number of matches played per year for all the years in IPL
function matchesPerYear(matches) {
  const matchCount = matches.reduce((seasonMatchCount, match) => {
    if (seasonMatchCount.hasOwnProperty(match.season))
      seasonMatchCount[match.season] += 1;
    else
      seasonMatchCount[match.season] = 1;
    return seasonMatchCount;
  }, {});
  return matchCount;
}

module.exports = matchesPerYear;