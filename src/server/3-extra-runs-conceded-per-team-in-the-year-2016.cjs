//Extra runs conceded per team in the given year
function extraRunsConceded(matches, deliveries, year) {
    const matchesOfSeason = matches.reduce((matchIdWithSeason, match) => {
        if (match.season == year)
            matchIdWithSeason[match.id] = match.season;
        return matchIdWithSeason;
    }, {});

    const extraRunsConcededByTeams = deliveries.reduce((team, delivery) => {
        if (matchesOfSeason.hasOwnProperty(delivery.match_id)) {
            if (team.hasOwnProperty(delivery.bowling_team))
                team[delivery.bowling_team] += Number(delivery.extra_runs);
            else
                team[delivery.bowling_team] = Number(delivery.extra_runs);
            //return team;
        }
        return team;
    }, {});

    return extraRunsConcededByTeams;
}

module.exports = extraRunsConceded;