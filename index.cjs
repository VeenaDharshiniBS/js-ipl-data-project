const path = require("path");
const csvFilePathMatches = path.join(__dirname, './src/data/matches.csv');
const csvFilePathDeliveries = path.join(__dirname, './src/data/deliveries.csv');
const fileSystem = require('fs');
const csv = require('csvtojson');

const matchesPerYear = require('./src/server/1-matches-per-year.cjs');
const matchesWonPerYearByTeams = require('./src/server/2-matches-won-per-team-per-year.cjs');
const extraRunsIn2016 = require('./src/server/3-extra-runs-conceded-per-team-in-the-year-2016.cjs');
const topEconomicalBowlersIn2015 = require('./src/server/4-top-10-economical-bowlers-2015.cjs');
const wonTossAndMatchPerTeam = require('./src/server/5-won-toss-and-match.cjs');
const playerWithMostPlayerOfMatchAward = require('./src/server/6-highest-playerOfMatch-season.cjs');
const strikeRateOfEachBatsman = require('./src/server/7.strike-rate-of-batsman-in-each-season.cjs');
const playerDismissedMost = require('./src/server/8-highest-player-dismissed-by-another-player.cjs');
const bestEconomyBowlerInSuperOvers = require('./src/server/9-bowler-with-best-economy-in-super-overs.cjs');
const numberOfMatchesPerCity = require('./src/server/10.number-of-matches-player-per-city.cjs');

csv()
  .fromFile(csvFilePathMatches)
  .then((jsonObjMatches) => {
    csv()
      .fromFile(csvFilePathDeliveries)
      .then((jsonObjDeliveries) => {

        const matchesPlayedPerYear = matchesPerYear(jsonObjMatches);
        const resmatchesPlayedPerYear = JSON.stringify(matchesPlayedPerYear);

        const matchesWonPerYearPerYear = matchesWonPerYearByTeams(jsonObjMatches);
        const resMatchesWonPerYearPerYear = JSON.stringify(matchesWonPerYearPerYear);

        const extraRunsConceded2016PerTeam = extraRunsIn2016(jsonObjMatches, jsonObjDeliveries, "2016");
        const resExtraRunsConceded2016PerTeam = JSON.stringify(extraRunsConceded2016PerTeam);

        const topEconomicalBowlers = topEconomicalBowlersIn2015(jsonObjMatches, jsonObjDeliveries, "2015");
        const resTopEconomicalBowlers = JSON.stringify(topEconomicalBowlers);

        const tossAndMatcheWinningCount = wonTossAndMatchPerTeam(jsonObjMatches);
        const resTossAndMatcheWinningCount = JSON.stringify(tossAndMatcheWinningCount);

        const highestPlayerOfMatchAwards = playerWithMostPlayerOfMatchAward(jsonObjMatches);
        const resHighestPlayerOfMatchAwards = JSON.stringify(highestPlayerOfMatchAwards);

        const strikeRatesPerBatsmanPerSeason = strikeRateOfEachBatsman(jsonObjMatches, jsonObjDeliveries);
        const resStrikeRatesPerBatsmanPerSeason = JSON.stringify(strikeRatesPerBatsmanPerSeason);

        const highestDismissedPlayer = playerDismissedMost(jsonObjDeliveries);
        const resHighestDismissedPlayer = JSON.stringify(highestDismissedPlayer);

        const bestEconomyInSuperOver = bestEconomyBowlerInSuperOvers(jsonObjDeliveries);
        const resBestEconomyInSuperOver = JSON.stringify(bestEconomyInSuperOver);

        const numberOfMatchPerCity = numberOfMatchesPerCity(jsonObjMatches);
        const resNumberOfMatchesPerCity = JSON.stringify(numberOfMatchPerCity);

        fileSystem.writeFile("./src/public/output/matchesPerYear.json", resmatchesPlayedPerYear, err => {
          if (err)
            console.log(err);
          else
            console.log('1.Number of matches played per year for all the years in IPL is done');
        });

        fileSystem.writeFile("./src/public/output/matchesWonPerTeamPerYear.json", resMatchesWonPerYearPerYear, err => {
          if (err)
            console.log(err);
          else
            console.log('2.Number of matches won per team per year in IPL is done');
        });

        fileSystem.writeFile("./src/public/output/extraRunsConceded2016.json", resExtraRunsConceded2016PerTeam, err => {
          if (err)
            console.log(err);
          else
            console.log('3.Extra runs conceded per team in the year 2016 is done');
        });

        fileSystem.writeFile("./src/public/output/top10EconomicalBowlers2015.json", resTopEconomicalBowlers, err => {
          if (err)
            console.log(err);
          else
            console.log('4.Top 10 economical bowlers in the year 2015 is done');
        });

        fileSystem.writeFile("./src/public/output/wonTossAndMatch.json", resTossAndMatcheWinningCount, err => {
          if (err)
            console.log(err);
          else
            console.log('5.Number of times each team won the toss and also won the match is done');
        });

        fileSystem.writeFile("./src/public/output/highestPlayerOfMatchAwards.json", resHighestPlayerOfMatchAwards, err => {
          if (err)
            console.log(err);
          else
            console.log('6.Player who has won the highest number of Player of the Match awards for each season is done');
        });

        fileSystem.writeFile("./src/public/output/strikeRateOfBatsmanInEachSeason.json", resStrikeRatesPerBatsmanPerSeason, err => {
          if (err)
            console.log(err);
          else
            console.log('7.The strike rate of a batsman for each season is done');
        });

        fileSystem.writeFile("./src/public/output/highestPlayerDismissedByAnotherPlayer.json", resHighestDismissedPlayer, err => {
          if (err)
            console.log(err);
          else
            console.log('8.The highest number of times one player has been dismissed by another player is done');
        });

        fileSystem.writeFile("./src/public/output/bowlerWithBestEconomyInSuperOvers.json", resBestEconomyInSuperOver, err => {
          if (err)
            console.log(err);
          else
            console.log('9.The bowler with the best economy in super overs is done');
        });

        fileSystem.writeFile("./src/public/output/numberOfMatchesPlayerPerCity.json", resNumberOfMatchesPerCity, err => {
          if (err)
            console.log(err);
          else
            console.log('10.The number of matches per city is done');
        });
      })
  })                                                                                                                                       